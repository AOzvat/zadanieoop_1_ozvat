#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>

typedef struct Zakaznik{
	char *meno;
	char *priezvisko;
	int stav_uctu;
	unsigned int pocet_chlebov;
	unsigned int pocet_zakuskov;
}ZAKAZNIK;

typedef struct Pekaren{
	char *nazov;
	char *sidlo;
	unsigned int hod_mzda;
	unsigned int hod_vykon;
	char *typ_chleba;
}PEKAREN;

typedef struct Cukraren{
	char *nazov;
	char *sidlo;
	unsigned int hod_mzda;
	unsigned int hod_vykon;
	char *typ_zakusku;
}CUKRAREN;

void pekarne (PEKAREN *p1,PEKAREN *p2,PEKAREN *p3,PEKAREN *p4,PEKAREN *p5,PEKAREN *p6){	
	p1->nazov="Jordan";
	p1->sidlo="Novi_Sad";
	p1->hod_mzda=4;
	p1->hod_vykon=16;
	p1->typ_chleba="psenicny";
	
	p2->nazov="Klas";
	p2->sidlo="Novi_Sad";
	p2->hod_mzda=2;
	p2->hod_vykon=12;
	p2->typ_chleba="psenicny";
	
	p3->nazov="Vale";
	p3->sidlo="Zrenjanjin";
	p3->hod_mzda=4;
	p3->hod_vykon=12;
	p3->typ_chleba="kukuricny";
	
	p4->nazov="Aroma";
	p4->sidlo="Zrenjanjin";
	p4->hod_mzda=2;
	p4->hod_vykon=10;
	p4->typ_chleba="psenicny";
	
	p5->nazov="Minja";
	p5->sidlo="Sremska_Mitrovica";
	p5->hod_mzda=4;
	p5->hod_vykon=12;
	p5->typ_chleba="chrono";
	
	p6->nazov="Ciao";
	p6->sidlo="Sremska_Mitrovica";
	p6->hod_mzda=2;
	p6->hod_vykon=6;
	p6->typ_chleba="kukuricny";
}

void cukrarne (CUKRAREN *c1,CUKRAREN *c2,CUKRAREN *c3,CUKRAREN *c4,CUKRAREN *c5,CUKRAREN *c6){
	c1->nazov="Tiffany";
	c1->sidlo="Novi_Sad";
	c1->hod_mzda=6;
	c1->hod_vykon=60;
	c1->typ_zakusku="coko";
	
	c2->nazov="Slatkis";
	c2->sidlo="Novi_Sad";
	c2->hod_mzda=4;
	c2->hod_vykon=48;
	c2->typ_zakusku="visna";
	
	c3->nazov="Marilin";
	c3->sidlo="Zrenjanjin";
	c3->hod_mzda=6;
	c3->hod_vykon=58;
	c3->typ_zakusku="coko";
	
	c4->nazov="Didi";
	c4->sidlo="Zrenjanjin";
	c4->hod_mzda=4;
	c4->hod_vykon=40;
	c4->typ_zakusku="mak";
	
	c5->nazov="Marcello";
	c5->sidlo="Sremska_Mitrovica";
	c5->hod_mzda=6;
	c5->hod_vykon=72;
	c5->typ_zakusku="coko";
	
	c6->nazov="Crush";
	c6->sidlo="Sremska_Mitrovica";
	c6->hod_mzda=4;
	c6->hod_vykon=36;
	c6->typ_zakusku="visna";
}

void print_pekaren (PEKAREN p){
	printf("%s - ",p.nazov);
	printf("%s ",p.sidlo);
	printf("[%s] \n",p.typ_chleba);
}

void print_cukraren (CUKRAREN c){
	printf("%s - ",c.nazov);
	printf("%s ",c.sidlo);
	printf("[%s] \n",c.typ_zakusku);
}

float vypocet_p (ZAKAZNIK *z,PEKAREN *p){
	float v;
	
	v=(float)z->pocet_chlebov/p->hod_vykon*p->hod_mzda;
		
	return v;
}

float vypocet_c (ZAKAZNIK *z,CUKRAREN *c){
	float v;
	
	v=(float)z->pocet_zakuskov/c->hod_vykon*c->hod_mzda;
	
	return v;
}

int main (void){
	ZAKAZNIK z;
	PEKAREN p1,p2,p3,p4,p5,p6;
 	CUKRAREN c1,c2,c3,c4,c5,c6;
	PEKAREN pek[6];
	CUKRAREN cuk[6];
	int i,v1,v2;
	char mesto_p[20],mesto_c[20];
 	
 	pekarne(&p1,&p2,&p3,&p4,&p5,&p6);
 	cukrarne(&c1,&c2,&c3,&c4,&c5,&c6);
 
 	
 	pek[0]=p1;
 	pek[1]=p2;
 	pek[2]=p3;
 	pek[3]=p4;
 	pek[4]=p5;
 	pek[5]=p6;
 	
 	cuk[0]=c1;
 	cuk[1]=c2;
 	cuk[2]=c3;
 	cuk[3]=c4;
 	cuk[4]=c5;
 	cuk[5]=c6;

 	printf("Pekarne a cukrarne.\n\n");
 	printf("Meno: ");
 	scanf("%s",&z.meno);
 	printf("Priezvisko: ");
 	scanf("%s",&z.priezvisko);
 	printf("Stav uctu: ");
 	scanf("%d",&z.stav_uctu);
 	printf("Kolko si chlebov prosite: ");
 	scanf("%u",&z.pocet_chlebov);
 	printf("Kolko si zakuskov prosite: ");
 	scanf("%u",&z.pocet_zakuskov);
 	printf("\n");

 	printf("Z ktoreho mesta ma byt pekaren? (Novi_Sad, Zrenjanjin, Sremska_Mitrovica)\n");
 	scanf("%s",&mesto_p);
 	printf("\n");

 	for (i=0;i<6;i++){
		v1=strcmp(mesto_p,pek[i].sidlo);
 		if (v1==0){
			if (z.stav_uctu>=vypocet_p(&z,&pek[i])){
				print_pekaren(pek[i]);
			}
		}
 	}
 	
 	printf("\n");
 	
	printf("Z ktoreho mesta ma byt cukraren? (Novi_Sad, Zrenjanjin, Sremska_Mitrovica)\n");
	scanf("%s",&mesto_c);
 	printf("\n");
 	
 	for (i=0;i<6;i++){
		v2=strcmp(mesto_c,cuk[i].sidlo);
 		if (v2==0){
			if (z.stav_uctu>=vypocet_c(&z,&cuk[i])){
				print_cukraren(cuk[i]);
			}
		}
 	}
 	
	return 0;	
}
